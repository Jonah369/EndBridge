package com.plan9.endbridge;

import net.fabricmc.api.ModInitializer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.minecraft.util.Identifier;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.MinecraftServer;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.text.Text;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents.ServerStopping;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents.ServerStarted;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.entity.Entity;
import java.io.File;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerTickEvents;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerTickEvents.StartTick;


public class EndBridge implements ModInitializer {
	// This logger is used to write text to the console and the log file.
	// It is considered best practice to use your mod id as the logger's name.
	// That way, it's clear which mod wrote info, warnings, and errors.
    public static final Logger LOGGER = LoggerFactory.getLogger("endbridge");
	public static MinecraftServer server;
	public static MatrixBotBridge MatrixBot;
	private static String MessageBuffer;
	private int tick_counter = 1;

	@Override
	public void onInitialize() {
		// This code runs as soon as Minecraft is in a mod-load-ready state.
		// However, some things (like resources) may still be uninitialized.
		// Proceed with mild caution.
		MatrixBot = new MatrixBotBridge();

		ServerLifecycleEvents.SERVER_STOPPING.register(new ServerStopping() {
			@Override
			public void onServerStopping(MinecraftServer server) {
				MatrixBot.SendMatrixMessage("Server Offline 🔴");
			}
		});

		ServerLifecycleEvents.SERVER_STARTING.register(serverInit -> {
            server = serverInit;
        });

        ServerLifecycleEvents.SERVER_STARTED.register(new ServerStarted() {
			@Override
			public void onServerStarted(MinecraftServer server) {
				MatrixBot.SendMatrixMessage("Server Online 🟢");
			}
		});

		ServerTickEvents.START_SERVER_TICK.register(new StartTick() {
			@Override
			public void onStartTick(MinecraftServer server) {
				if (tick_counter == MatrixBot.get_tick_iter())
				{
					MatrixBot.FetchMatrixMessages();
					tick_counter = 0;
				}


				MessageBuffer = MatrixBot.ReadMessage();
				if (MessageBuffer != "" && MessageBuffer != null)
					SendMessages(MessageBuffer);

				tick_counter++;
			}
		});
	}

	public static void onJoin(PlayerEntity player) {
		MatrixBot.SendMatrixMessage(player.getName().getString() + " joined the server");
	}

	public static void onLeave(PlayerEntity player) {
		MatrixBot.SendMatrixMessage(player.getName().getString() + " left the server");
	}

	public static void SendMessages(String message) {
		server.getPlayerManager().broadcast(Text.of(message), false);
	}

	public static void ReceiveMessage(ServerPlayerEntity player, String message)
	{
		MatrixBot.SendMatrixMessage("<"+player.getName().getString()+"> "+message);
	}

	public static void onReceiveAcheivement()
	{
        //TODO: achievement message
	}

	public static void OnDeathMessage(String DeathMessage)
	{
		//TODO: Death message
	}

}