package com.plan9.endbridge;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.PriorityQueue;
import java.io.IOException;

import net.fabricmc.loader.impl.FabricLoaderImpl;
import java.nio.file.Files;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.MalformedJsonException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.time.Duration;
import java.net.http.*;
import java.util.UUID;


public class MatrixBotBridge { 
    private String auth_Token;
    private String home_server;
    private String room_ID;
    private int tick_iter;

    private String last_message_token;
    private boolean isconfLoaded = false;

    private PriorityQueue<String> ReceivedMessage_Buffer;
    private String bot_user_id;
    
    public MatrixBotBridge()
    {
        this.ReceivedMessage_Buffer = new PriorityQueue<String>();
        Logger LOGGER = LoggerFactory.getLogger("endbridge");
        JsonObject jsonObj;
        File conf = new File(FabricLoaderImpl.INSTANCE.getConfigDir() + "/EndBridge.json");
        
        if (!conf.exists())
        {
            jsonObj = new JsonObject();
            try {
                conf.createNewFile();
                conf.setWritable(true);

                jsonObj.addProperty("AccessToken", "");
                jsonObj.addProperty("HomeServer", "");
                jsonObj.addProperty("RoomID", "");
                jsonObj.addProperty("Tick_Iterations", 50);
    
                try (PrintWriter f = new PrintWriter(conf)) {
                    f.println(jsonObj.toString());
                 } catch (FileNotFoundException e) {
                   e.printStackTrace();
                 }
            } catch (IOException e) {
                throw new RuntimeException("Cannot Create Config File.",e);
            }
        } else {
            try {
                jsonObj = (JsonObject) JsonParser.parseString(new String(Files.readAllBytes(conf.toPath())));
            } catch (IOException e) {
                throw new RuntimeException("Cannot Read From Config File.",e);
            }
            
            this.auth_Token = jsonObj.get("AccessToken").getAsString();
            this.home_server = jsonObj.get("HomeServer").getAsString();
            this.room_ID = jsonObj.get("RoomID").getAsString();
            this.tick_iter = jsonObj.get("Tick_Iterations").getAsInt();

            this.isconfLoaded = true;
        }

        if (this.isconfLoaded)
        {
            try {
                this.last_message_token = ((JsonObject) JsonParser.parseString(HttpClient.newHttpClient()
                .send(HttpRequest.newBuilder()
                .uri(URI.create(this.home_server+"/_matrix/client/v3/rooms/"+this.room_ID+"/messages?dir=b&limit=1"))
                .timeout(Duration.ofMinutes(1))
                .header("Authorization", "Bearer "+this.auth_Token)
                .GET()
                .build(), HttpResponse.BodyHandlers.ofString()).body())).get("start").getAsString();

                this.bot_user_id = ((JsonObject) JsonParser.parseString(HttpClient.newHttpClient()
                .send(HttpRequest.newBuilder()
                .uri(URI.create(this.home_server+"/_matrix/client/v3/account/whoami"))
                .timeout(Duration.ofMinutes(1))
                .header("Authorization", "Bearer "+this.auth_Token)
                .GET()
                .build(), HttpResponse.BodyHandlers.ofString()).body())).get("user_id").getAsString();
            } catch (MalformedJsonException e) {
                EndBridge.LOGGER.info("MalformedJsonException while fetching messages\n"+e.getMessage());
            } catch (IOException e) {
                EndBridge.LOGGER.info("IOException while extracting last_message_token\n"+e.getMessage());
            } catch (InterruptedException e) {
                EndBridge.LOGGER.info("InterruptedException while extracting last_message_token\n"+e.getMessage());
            } 
        }
    }

    public void SendMatrixMessage(String message)
    {
        HttpRequest request = HttpRequest.newBuilder()
        .uri(URI.create(this.home_server+"/_matrix/client/v3/rooms/"+this.room_ID+"/send/m.room.message/"+UUID.randomUUID().toString().replaceAll("_", "").replaceAll("-", "")))
        .timeout(Duration.ofMinutes(1))
        .header("Authorization", "Bearer "+this.auth_Token)
        .PUT(HttpRequest.BodyPublishers.ofString("{\"msgtype\": \"m.text\",\"body\": \""+message+"\"}"))
        .build();

        HttpClient.newHttpClient().sendAsync(request, HttpResponse.BodyHandlers.ofString());
    }
    
    public void FetchMatrixMessages()
    {
        //TODO: Clean the code i'm tired af
        try {
            JsonObject FetchBuffer = (JsonObject) JsonParser.parseString(HttpClient.newHttpClient()
            .send(HttpRequest.newBuilder()
            .uri(URI.create(this.home_server+"/_matrix/client/v3/rooms/"+this.room_ID+"/messages?dir=f&from="+this.last_message_token+"&filter=%7B%22types%22:%5B%22m.room.message%22%5D,%22contains_url%22:false,%22not_senders%22:%5B%22"+this.bot_user_id+"%22%5D%7D&limit=5"))
            .timeout(Duration.ofMinutes(1))
            .header("Authorization", "Bearer "+this.auth_Token)
            .GET()
            .build(), HttpResponse.BodyHandlers.ofString()).body());

            for(JsonElement i : FetchBuffer.get("chunk").getAsJsonArray())
            {
                JsonObject TempI = i.getAsJsonObject();
                this.ReceivedMessage_Buffer.add("<(M)"+TempI.get("sender").getAsString().split(":")[0].substring(1)+"> "+ TempI.get("content").getAsJsonObject().get("body").getAsString());
            }
            
            if (FetchBuffer.get("end")!=null)
                this.last_message_token = FetchBuffer.get("end").getAsString();

        } catch (MalformedJsonException e) {
            EndBridge.LOGGER.info("MalformedJsonException while fetching messages\n"+e.getMessage());
        } catch (IOException e) {
            EndBridge.LOGGER.info("IOException while Fetching Messages.\n"+e.getMessage());
        } catch (InterruptedException e) {
            EndBridge.LOGGER.info("InterruptedException while Fetching Messages.\n"+e.getMessage());
        } catch (Exception e) {
            EndBridge.LOGGER.info("Exception while Fetching Messages.\n"+e.getMessage());
        }
    }

    public String ReadMessage()
    {
        if (this.ReceivedMessage_Buffer.size() == 0)
            return "";
        return this.ReceivedMessage_Buffer.poll();
    }

    public int get_tick_iter()
    {
        return this.tick_iter;
    }

}
