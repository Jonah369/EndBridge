package com.plan9.endbridge.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import com.plan9.endbridge.EndBridge;

import net.minecraft.network.packet.c2s.play.ChatMessageC2SPacket;
import net.minecraft.server.network.ServerPlayNetworkHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import org.apache.commons.lang3.StringUtils;

@Mixin(ServerPlayNetworkHandler.class)
public class ChatHandlerMixin {
	
	@Shadow public ServerPlayerEntity player;

	@Inject(method = "onChatMessage(Lnet/minecraft/network/packet/c2s/play/ChatMessageC2SPacket;)V", at = @At("TAIL"))
	public void onChatMessage(ChatMessageC2SPacket packet, CallbackInfo callbackInfo) {
		String string = StringUtils.normalizeSpace(packet.chatMessage());
		if (!string.startsWith("/")) {
			EndBridge.ReceiveMessage(player, string);
		}
	}
}
