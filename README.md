![](./src/main/resources/assets/icon.png)

# EndBridge
EndBridge is a [Matrix](https://matrix.org) to Minecraft chat bridge, built with Fabric. It allows Minecraft players to communicate with Matrix users with the help of the [Matrix Client-Server API](https://spec.matrix.org/v1.8/client-server-api/).<br/>
Download the mod either from [releases](https://gitea.com/Jonah369/EndBridge/releases) or [Modrinth](https://modrinth.com/mod/endbridge).

![](./img/Screen1.png)

# Configuration
Find the config file named `EndBridge.json` (generated after running the mod for the first time).

Make sure to create the bot account and invite it to the desired room.
### Parameters
- **AccessToken :** Generate one using [Matrix API](https://spec.matrix.org/v1.8/client-server-api/#login) or extract it using your favorite client.
- **HomeServer :** The API URL for the homeserver where the bot is registered, for example `https://matrix.domain.com`. If you'd like to make a connection to a Matrix homeserver running locally on the same system, using `http://127.0.0.1:8008`is recommended.
- **RoomID :** The room ID of the room where the bot will run.
- **Tick_Iterations :** Number of ticks to fetch for messages, we suggest 50 or above (check with your homeserver administrators).

# Compiling from source
1. Clone this project.
2. Run `./gradlew build` on the terminal.
3. You'll find the compiled file at `./build/libs/` named `endbridge-[version].jar`.

# Contribution
We welcome all contributors to make a pull request.<br/>
Keep it simple ~~stupid~~, follow the documentation (if one is made), and enjoy the mod.

# Special Thanks

**Ophthal** for making the icon.<br/>
**Cat** for trolling.<br/>
**Java** for losing my sleep.<br/>
<br/><br/>
Contact me on [Matrix](https://matrix.to/#/@clarkson:plan9.rocks).
